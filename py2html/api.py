class Tag:
    def __init__(self, _name: str, _text: str = "", **attrs: dict):
        self.name = _name
        self.attrs = attrs
        self.children = []
        self.text = _text

    def _is_simple_tag(self):
        return not (self.text or self.children)

    def _open_tag(self):
        if not self.name:
            return ""

        open_tag = f"<{self.name}"

        if self._is_simple_tag():
            return open_tag

        if self.attrs:
            return open_tag

        return open_tag + ">"

    def _close_tag(self):
        if not self.name:
            return ""

        close_tag = ""
        if self._is_simple_tag():
            close_tag += " />"
        else:
            close_tag += f"</{self.name}>"

        return close_tag

    def _compile_attrs(self):
        word_list = []
        for key, value in self.attrs.items():
            if value:
                word_list.append(f"{key}=\"{value}\"")
            else:
                word_list.append(key)

        return " " + " ".join(word_list)

    def _compile_children(self):
        tag_list = []
        for child in self.children:
            tag_list.append(child.compile())

        return tag_list

    def add_child(self, child: "Tag") -> None:
        self.children.append(child)

    def compile(self):
        tag_list = []
        tag_list.append(self._open_tag())

        if self.attrs:
            tag_list.append(self._compile_attrs())
            if not self._is_simple_tag():
                tag_list.append(">")

        if self.text:
            tag_list.append(self.text)

        if self.children:
            tag_list.extend(self._compile_children())

        tag_list.append(self._close_tag())

        return "".join(tag_list)
