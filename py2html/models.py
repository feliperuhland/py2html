from py2html import api


class HTML:
    def __init__(self, doctype=True, **attrs):
        self._doctype = doctype
        self._tag = api.Tag("html", **attrs)

    def add_child(self, child: api.Tag):
        self._tag.add_child(child)

    def compile(self):
        result = self._tag.compile()
        if self._doctype:
            result = "<!DOCTYPE html>" + result

        return result
