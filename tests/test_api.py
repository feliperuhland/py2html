import unittest

from py2html.api import Tag


class ApiTestCase(unittest.TestCase):
    def test_html_tag(self):
        html = Tag("html")

        self.assertEqual("<html />", html.compile())

    def test_html_and_head(self):
        html = Tag("html")
        html.add_child(Tag("head"))

        self.assertEqual("<html><head /></html>", html.compile())

    def test_html_and_head_and_meta(self):
        html = Tag("html")
        head = Tag("head")
        head.add_child(Tag("meta", charset="utf-8"))
        html.add_child(head)

        self.assertEqual(
            "<html><head><meta charset=\"utf-8\" /></head></html>",
            html.compile()
        )

    def test_complex_tag(self):
        tag = Tag("button", "Click me", onclick="myFunction()")

        self.assertEqual(
            '<button onclick="myFunction()">Click me</button>',
            tag.compile()
        )

    def test_tag_inside_text(self):
        expected = "<p>Hello, <b>world</b>!</p>"

        p = Tag("p")
        p.add_child(Tag("", "Hello, "))
        p.add_child(Tag("b", "world"))
        p.add_child(Tag("", "!"))

        self.assertEqual(expected, p.compile())

    def test_two_tags_inside_text(self):
        expected = "<p>Hello, <i><b>world</b></i>!</p>"

        p = Tag("p")
        p.add_child(Tag("", "Hello, "))
        italic = Tag("i")
        italic.add_child(Tag("b", "world"))
        p.add_child(italic)
        p.add_child(Tag("", "!"))

        self.assertEqual(expected, p.compile())
