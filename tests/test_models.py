import unittest
from py2html.api import Tag
from py2html.models import HTML


class HTMLTestCase(unittest.TestCase):
    def test_html(self):
        html = HTML()
        head = Tag("head")
        head.add_child(Tag("title", "hello"))
        body = Tag("body", "world")
        html.add_child(head)
        html.add_child(body)
        expected = "<!DOCTYPE html><html><head><title>hello</title>" \
            "</head><body>world</body></html>"

        self.assertEqual(expected, html.compile())

    def test_bootstrap_getting_started(self):
        expected = """<!DOCTYPE html><html lang="en"><head>""" \
            """<meta charset="utf-8" /><meta name="viewport" """ \
            """content="width=device-width, initial-scale=1" /><title>""" \
            """Bootstrap demo</title><link href="https://cdn.jsdelivr.net""" \
            """/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" """ \
            """rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVM""" \
            """WSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" """ \
            """crossorigin="anonymous" /></head><body><h1>Hello, world!""" \
            """</h1><script src="https://cdn.jsdelivr.net/npm/bootstrap""" \
            """@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity""" \
            """="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdg""" \
            """RZgiwxhTBTkF7CXvN" crossorigin="anonymous" /></body></html>"""

        html = HTML(lang="en")
        head = Tag("head")
        head.add_child(Tag("meta", charset="utf-8"))
        head.add_child(Tag(
            "meta", **{"name": "viewport",
                       "content": "width=device-width, initial-scale=1"}
        ))

        head.add_child(Tag("title", "Bootstrap demo"))
        head.add_child(Tag(
            "link",
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/"
                 "dist/css/bootstrap.min.css",

            rel="stylesheet",
            integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/"
                      "Jr59b6EGGoI1aFkw7cmDA6j6gD",
            crossorigin="anonymous"))

        body = Tag("body")
        body.add_child(Tag("h1", "Hello, world!"))
        body.add_child(Tag(
            "script",
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/"
                "dist/js/bootstrap.bundle.min.js",
            integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+"
                      "nuZB+EYdgRZgiwxhTBTkF7CXvN",
            crossorigin="anonymous"))

        html.add_child(head)
        html.add_child(body)

        self.maxDiff = None
        self.assertEqual(expected, html.compile())
